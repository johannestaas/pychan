import urllib2
from hashlib import sha256
from base64 import urlsafe_b64encode as b64enc

from django.db import models

from django.utils.html import strip_tags

MAX_IMAGE_SIZE = 2**21

class Thread(models.Model):
    def op(self):
        return self.posts.first()

class ImageURL(models.Model):
    img_url = models.CharField(max_length=2048, unique=True, default='')
    size = models.IntegerField(default=0)
    hash = models.CharField(max_length=43)

    def peek_url(self):
        file = urllib2.urlopen(self.img_url)
        size = file.headers.get('content-length')
        if size:
            self.size = int(size)
            if self.size > MAX_IMAGE_SIZE:
                return self.size
            max_size = min(self.size, 4096)
        else:
            max_size = 4096
        req = urllib2.Request(self.img_url, 
            headers={'Range': 'bytes=0-%d' % max_size})
        data = urllib2.urlopen(req).read(max_size)[:max_size]
        if not size:
            self.size = len(data)
        if self.size > MAX_IMAGE_SIZE:
            # Dont hash large file
            return self.size
        self.hash = b64enc(sha256(data).digest())[:43]
        return self.size
    
class Post(models.Model):
    text = models.CharField(max_length=4096, default='')
    img = models.ForeignKey(ImageURL, null=True)
    date = models.DateTimeField()
    thread = models.ForeignKey(Thread, db_index=True, related_name='posts',
        default=None)
    submitter = models.CharField(max_length=43)
    src = models.CharField(max_length=64, default='')

    def load_img(self, url):
        img, created = ImageURL.objects.get_or_create(img_url=url)
        self.img = img
        if created:
            img.peek_url()
        return (self.img.size <= MAX_IMAGE_SIZE)

    def url(self):
        return strip_tags(self.img.img_url)

    def msg(self):
        newlines = []
        for line in self.text.splitlines():
            if line.startswith('>'):
                newlines += [('greentext', strip_tags(line))]
            else:
                newlines += [('', strip_tags(line))]
        return newlines

